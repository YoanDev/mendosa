import './App.css';
import './components/Buttons/CSS/Standard.css'
import Tests from './pages/Tests';

function App() {
  return (
    <>
      <Tests />
    </>
  );
}

export default App;
