import React from 'react'
import {FaHammer} from 'react-icons/fa'
const Center = props => {
    return (
        <>
            <div className="test-container">
                {props.src &&(
                    <div className="box-img">
                     <img src={props.src} alt="logo"/>
                     <h2>Mendosa Custom <FaHammer/> : {props.custom}</h2>
                     </div>
                )}
                
                {props.children}
            </div>
        </>
    )
}

export default Center
