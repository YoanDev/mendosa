import React, { useState } from "react";
import Button01 from "../../Buttons/Button01";
import "./ButtonsContainer.css";

//version: primera, peligro, advertencia, ligera, oscura, valida

const ButtonsContainer = () => {
  const [state, setState] = useState("");
  const [paragraphe, setParagraphe] = useState("");

  return (
    <>
      <div className="buttons-container">
        <div className="box-buttons">
          <div className="button-desc">
            <p>{paragraphe}</p>
          </div>
          <div className="buttons-list">
            <div className="square">
              <Button01
                onClick={() => setParagraphe(`<Button01 className="btn primera active" text="primera"  />`) }
                onMouse={() => setState("Primera")}
                onLeave={() => setState("")}
                className="btn primera active"
                text="primera"
              />
            </div>
            <div className="square">
              <Button01
                onClick={()=> setParagraphe(`<Button01 className="btn peligro active" text="peligro"  />`) }
                onMouse={() => setState("Peligro")}
                onLeave={() => setState("")}
                className="btn peligro active"
                text="peligro"
              />
            </div>
            <div className="square">
              <Button01
                onClick={() => setParagraphe(`<Button01 className="btn valida active" text="valida"  />`) }
                onMouse={() => setState("Valida")}
                onLeave={() => setState("")}
                className="btn valida active"
                text="valida"
              />
            </div>
            <div className="square">
              <Button01
                onClick={() => setParagraphe(`<Button01 className="btn advertencia active" text="advertencia"  />`) }
                onMouse={() => setState("Advertencia")}
                onLeave={() => setState("")}
                className="btn advertencia active"
                text="advertencia"
              />
            </div>
            <div className="square">
              <Button01
                onClick={() => setParagraphe(`<Button01 className="btn ligera active" text="ligera"  />`) }
                onMouse={() => setState("Ligera")}
                onLeave={() => setState("")}
                className="btn ligera active"
                text="ligera"
              />
            </div>
            <div className="square">
              <Button01
                onClick={() => setParagraphe(`<Button01 className="btn oscura active" text="oscura"  />`) }
                onMouse={() => setState("Oscura")}
                onLeave={() => setState("")}
                className="btn oscura active"
                text="oscura"
              />
            </div>
          </div>
          <div className="content">
            <div>{state}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ButtonsContainer;
