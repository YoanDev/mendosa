import React, { useState } from "react";
import Avatar02All from "./Avatar02All";
import Avatars01All from "./Avatars01All";
import "./AvatarsContainer.css";
import { JsonDatas } from "./JsonDatas";

const AvatarsContainer = (props) => {
  const [code01, setCode01] = useState("");
  const [code02, setCode02] = useState("");
  const [datas, setDatas] = useState(JsonDatas);

  return (
    <>
      <div className="section-avatars">
        <div className="avatars-container">
          <div className="box-avatars1">
            <div className="title-avatars">
              <h2>Avatar01: </h2>
              <h5>{code01}</h5>
            </div>
            <Avatars01All
              onClickPrimera={() =>
                setCode01(
                  datas.map((data) => data.avatar01.map((data) => data.primera))
                )
              }
              onClickPeligro={() =>
                setCode01(
                  datas.map((data) => data.avatar01.map((data) => data.peligro))
                )
              }
              onClickAdvertencia={() =>
                setCode01(
                  datas.map((data) =>
                    data.avatar01.map((data) => data.advertencia)
                  )
                )
              }
              onClickValida={() =>
                setCode01(
                  datas.map((data) => data.avatar01.map((data) => data.valida))
                )
              }
              onClickLigera={() =>
                setCode01(
                  datas.map((data) => data.avatar01.map((data) => data.ligera))
                )
              }
              onClickOscura={() =>
                setCode01(
                  datas.map((data) => data.avatar01.map((data) => data.oscura))
                )
              }
            />
          </div>
          <div className="box-avatars1">
            <div className="title-avatars">
              <h2>Avatar02: </h2>
              <h5>{code02}</h5>
            </div>
            <Avatar02All
              onClickPrimera={() =>
                setCode02(
                  datas.map((data) => data.avatar02.map((data) => data.primera))
                )
              }
              onClickPeligro={() =>
                setCode02(
                  datas.map((data) => data.avatar02.map((data) => data.peligro))
                )
              }
              onClickAdvertencia={() =>
                setCode02(
                  datas.map((data) =>
                    data.avatar02.map((data) => data.advertencia)
                  )
                )
              }
              onClickValida={() =>
                setCode02(
                  datas.map((data) => data.avatar02.map((data) => data.valida))
                )
              }
              onClickLigera={() =>
                setCode02(
                  datas.map((data) => data.avatar02.map((data) => data.ligera))
                )
              }
              onClickOscura={() =>
                setCode02(
                  datas.map((data) => data.avatar02.map((data) => data.oscura))
                )
              }
            />
          </div>
          <div className="box-avatars2"></div>
        </div>
      </div>
    </>
  );
};

export default AvatarsContainer;
