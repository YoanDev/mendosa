import React from "react";
import Avatar02 from "../../Avatar/Avatar02/Avatar02";
import Image1 from "../../../img/naruto.png";
import Image2 from "../../../img/sasuke.png";
import Galaxy from "../../../img/galaxy.jpg";

const Avatar02All = (props) => {
  return (
    <>
      <Avatar02
        onClick={props.onClickPrimera}
        bg1={Image1}
        bg2={Galaxy}
        className="avatar02 primera"
      />
      <Avatar02
        onClick={props.onClickPeligro}
        bg1={Image1}
        bg2={Galaxy}
        className="avatar02 peligro"
      />
      <Avatar02
        onClick={props.onClickValida}
        bg1={Image1}
        bg2={Galaxy}
        className="avatar02 valida"
      />
      <Avatar02
        onClick={props.onClickAdvertencia}
        bg1={Image2}
        bg2={Galaxy}
        className="avatar02 advertencia"
      />
      <Avatar02
        onClick={props.onClickOscura}
        bg1={Image2}
        bg2={Galaxy}
        className="avatar02 oscura"
      />
      <Avatar02
        onClick={props.onClickLigera}
        bg1={Image2}
        bg2={Galaxy}
        className="avatar02 ligera"
      />
    </>
  );
};

export default Avatar02All;
