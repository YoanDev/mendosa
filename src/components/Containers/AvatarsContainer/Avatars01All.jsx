import React from "react";
import Avatar01 from "../../Avatar/Avatar01/Avatar01";
import Image1 from "../../../img/naruto.png";
import Image2 from "../../../img/sasuke.png";
import Metal from "../../../img/metal.jpg";

const Avatars01All = (props) => {
  return (
    <>
      <Avatar01
        className="avatar primera"
        backgroundImages={Image1}
        contour="contour primera"
        contourImg={Metal}
        onClick={props.onClickPrimera}
      />
      <Avatar01
        className="avatar peligro"
        backgroundImages={Image1}
        contour="contour peligro"
        contourImg={Metal}
        onClick={props.onClickPeligro}
      />
      <Avatar01
        className="avatar valida"
        backgroundImages={Image1}
        contour="contour valida"
        contourImg={Metal}
        onClick={props.onClickValida}
      />
      <Avatar01
        className="avatar advertencia"
        backgroundImages={Image2}
        contour="contour advertencia"
        contourImg={Metal}
        onClick={props.onClickAdvertencia}
      />
      <Avatar01
        className="avatar oscura"
        backgroundImages={Image2}
        contour="contour oscura"
        contourImg={Metal}
        onClick={props.onClickOscura}
      />
      <Avatar01
        className="avatar ligera"
        backgroundImages={Image2}
        contour="contour ligera"
        contourImg={Metal}
        onClick={props.onClickLigera}
      />
    </>
  );
};

export default Avatars01All;
