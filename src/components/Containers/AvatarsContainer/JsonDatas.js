export const JsonDatas = [
    {
        "avatar01": [{
            "primera": '<Avatar01 className="avatar primera" backgroundImages={Image1} contour="contour primera" contourImg={Metal}/>',
            "peligro": '<Avatar01 className="avatar peligro" backgroundImages={Image1} contour="contour peligro" contourImg={Metal}/>',
            "advertencia": '<Avatar01 className="avatar advertencia" backgroundImages={Image1} contour="contour advertencia" contourImg={Metal}/>',
            "valida": '<Avatar01 className="avatar valida" backgroundImages={Image1} contour="contour valida" contourImg={Metal}/>',
            "ligera": '<Avatar01 className="avatar ligera" backgroundImages={Image1} contour="contour ligera" contourImg={Metal}/>',
            "oscura": '<Avatar01 className="avatar oscura" backgroundImages={Image1} contour="contour oscura" contourImg={Metal}/>',
        }],
        "avatar02": [{
            "primera": '<Avatar02 className="avatar02 primera" backgroundImages={Image1} contour="contour primera" contourImg={Metal}/>',
            "peligro": '<Avatar02 className="avatar02 peligro" backgroundImages={Image1} contour="contour peligro" contourImg={Metal}/>',
            "advertencia": '<Avatar02 className="avatar02 advertencia" backgroundImages={Image1} contour="contour advertencia" contourImg={Metal}/>',
            "valida": '<Avatar02 className="avatar02 valida" backgroundImages={Image1} contour="contour valida" contourImg={Metal}/>',
            "ligera": '<Avatar02 className="avatar02 ligera" backgroundImages={Image1} contour="contour ligera" contourImg={Metal}/>',
            "oscura": '<Avatar02 className="avatar02 oscura" backgroundImages={Image1} contour="contour oscura" contourImg={Metal}/>',
        }],

    }
        
] 