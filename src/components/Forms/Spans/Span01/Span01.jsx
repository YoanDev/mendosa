import React from 'react'
import './CSS/Span01.css'

const Span01 = (props) => {
    return (
        <>
            <span className={props.spanClass ? props.spanClass : "span-01"}>{props.children}</span>
        </>
    )
}

export default Span01
