import React from 'react'
import Input01 from '../../Inputs/Input01/Input01'
import './CSS/Form01.css'
import './CSS/Standard.css'

const Form01 = (props) => {
    return (
        <>
            <form className={props.formClass ? props.formClass : "form-01"} action="" method={props.method}>
                <h2 className="form-01-title">{props.title}</h2>
               {props.children}
            </form>
            
        </>
    )
}

export default Form01
