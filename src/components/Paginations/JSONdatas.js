export const JSONdata = [
    {
        "id": "1",
        "name": "John Doe"
    },
    {
        "id": "2",
        "name": "Jin Doe"
    },
    {
        "id": "3",
        "name": "Derek Doe"
    },
    {
        "id": "4",
        "name": "Bonnie Doe"
    },
    {
        "id": "5",
        "name": "Duck Doe"
    },
    {
        "id": "6",
        "name": "Lambda Doe"
    },
    {
        "id": "7",
        "name": "Mike Doe"
    },
    {
        "id": "8",
        "name": "Alex Doe"
    },
    {
        "id": "9",
        "name": "Ramad Doe"
    },
    {
        "id": "10",
        "name": "Pierrick Doe"
    },
    {
        "id": "11",
        "name": "Jane Doe"
    },
]