import React from "react";

import "./CSS/PaginateList.css";
const PaginateList = (props) => {
  return (
    <>
      <div className="paginate-section">
        <div className={props.className}>{props.children}</div>
      </div>
    </>
  );
};

export default PaginateList;
