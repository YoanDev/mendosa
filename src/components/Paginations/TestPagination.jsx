import React from "react";
import Center from "../Containers/Center";
import DisplayPagination from "./DisplayPagination";
import { JSONdata } from "./JSONdatas";
import dragon from '../../img/dragon.png'
const TestPagination = () => {
  const usersPerPage = 5
  return (
    <>
      <Center src={dragon}>
        <DisplayPagination
          datas={JSONdata}
          usersPerPage={usersPerPage}
          className="paginate-container oscura"
          btnClass="paginationBttns ligera"
          btnClassActive="paginationBttnActive ligera"
        />
        <DisplayPagination
          datas={JSONdata}
          usersPerPage={usersPerPage}
          className="paginate-container ligera"
          btnClass="paginationBttns oscura"
          btnClassActive="paginationBttnActive peligro"
        />
        
      </Center>
    </>
  );
};

export default TestPagination;
