import React, { useState } from "react";
import { JSONdata } from "./JSONdatas";
import ReactPaginate from "react-paginate";
import PaginateList from "./PaginateList";
import './CSS/Standard.css'
import './CSS/PaginationBtns.css'
const DisplayPagination = (props) => {
  const [users, setUsers] = useState(props.datas);
  const [pageNumber, setPageNumber] = useState(0);

  const usersPerPage = props.usersPerPage;
  const pagesVisited = pageNumber * usersPerPage;

  const displayUsers = users
    .slice(pagesVisited, pagesVisited + usersPerPage)
    .map((user) => {
      return (
        <div className="user" key={user.id}>
          <h2>{user.name}</h2>
        </div>
      );
    });

    const pageCount = Math.ceil(users.length / usersPerPage)

    const changePage = ({selected}) => {
        setPageNumber(selected)
    }
  return (
    <>
        <PaginateList className={props.className}>

            {displayUsers}
          <ReactPaginate
          previousLabel={"Previous"}
          nextLabel={"Next"}
          pageCount={pageCount}
          onPageChange={changePage}
          containerClassName={props.btnClass ? props.btnClass : "paginationBttns"}
          previousLinkClassName={"previousBttn"}
          nextLinkClassName={"nextBttn"}
          disabled={"paginationDisabled"}
          activeClassName={props.btnClassActive ? props.btnClassActive : "paginationBttnActive"}
          />
        </PaginateList>
    </>
  );
};

export default DisplayPagination;
