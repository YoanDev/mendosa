import React from "react";
import "./CSS/Button01.css";

//version: primera, peligro, advertencia, ligera, oscura, valida

const Button01 = props => {
  return (
    <>
      <div className={props.className} onMouseEnter={props.onMouse} onMouseLeave={props.onLeave} onClick={props.onClick}>
        <div>{props.text}</div>
      </div>
    </>
  );
};

export default Button01;
