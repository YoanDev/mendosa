import React from "react";
import "./CSS/Avatar02.css";
import "./CSS/Standard.css";

const Avatar02 = (props) => {
  return (
    <>
      <div
        className={props.className ? props.className : "avatar02"}
        style={{ backgroundImage: `url(${props.bg1}), url(${props.bg2})` }}
        onClick={props.onClick}
      ></div>
    </>
  );
};

export default Avatar02;
