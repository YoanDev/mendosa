import React from "react";
import "./CSS/Avatar.css";
import "./CSS/Standard.css";

const Avatar01 = (props) => {
  return (
    <>
      <div
        className={props.contour ? props.contour : "contour"}
        style={{ backgroundImage: `url(${props.contourImg})` }}
      >
        <div
          className={props.className ? props.className : "avatar"}
          style={{ backgroundImage: `url(${props.backgroundImages})` }}
          onClick={props.onClick}
        ></div>
      </div>
    </>
  );
};

export default Avatar01;
