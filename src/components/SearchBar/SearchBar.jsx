import React, { useState } from "react";
import "./CSS/SearchBar.css";
import "./CSS/Standard.css";

const SearchBar = (props) => {
  const [searchTerm, setSearchTerm] = useState("");

  const targetSearch = (e) => {
    setSearchTerm(e.target.value);
  };

  return (
    <>
      <div className="search-container">
        <input
          type="text"
          onChange={targetSearch}
          placeholder={searchTerm}
          className={props.searchClass ? props.searchClass : "search-bar"}
        />
        <div className="datas-container">
          {props.datas
            .filter((val) => {
              if (searchTerm == "") {
                return val;
              } else if (val.name.includes(searchTerm)) {
                return val;
              }
            })
            .map((val, index) => {
              return (
                <>
                  <div className="search-datas" key={index}>
                    <div>{val.name}</div>
                  </div>
                </>
              );
            })}
        </div>
      </div>
    </>
  );
};

export default SearchBar;
