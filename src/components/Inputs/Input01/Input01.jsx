import React from "react";
import "./CSS/Input01.css";
import "./CSS/Standard.css";

const Input01 = (props) => {
  return (
    <>
      <input
        className={props.inputClass ? props.inputClass : "input-01"}
        type={props.type}
        name={props.name}
        validation={props.validation}
        onChange={props.onChange}
        value={props.value}
      />
    </>
  );
};

export default Input01;
