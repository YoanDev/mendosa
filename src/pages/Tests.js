import React from "react";
import Avatar01 from "../components/Avatar/Avatar01/Avatar01";
import Center from "../components/Containers/Center";
import dragon from "../img/dragon.png";
import Image1 from '../img/naruto.png'
import Image2 from '../img/sasuke.png'
import Metal from '../img/metal.jpg'
import Fire from '../img/fire.jpg'
import Galaxy from '../img/space.jpg'
import Avatar02 from "../components/Avatar/Avatar02/Avatar02";
import AvatarsContainer from "../components/Containers/AvatarsContainer/AvatarsContainer";
import ButtonsContainer from "../components/Containers/ButtonsContainer/ButtonsContainer";
import TestPagination from "../components/Paginations/TestPagination";
import PaginateList from "../components/Paginations/PaginateList";
import DisplayPagination from "../components/Paginations/DisplayPagination";
import Form01 from "../components/Forms/Form01/Form01";
import Input01 from "../components/Inputs/Input01/Input01";
import Span01 from "../components/Forms/Spans/Span01/Span01";
import SearchBar from "../components/SearchBar/SearchBar";
import { SearchJSONdata } from "../components/SearchBar/SearchJSONdatas";

const Tests = () => {
  return (
    <>
        <Center src={dragon} custom="Pagination">
          <TestPagination />
        </Center>
        <Center src={dragon} custom="Pagination">
          <SearchBar datas={SearchJSONdata} searchClass="search-bar primera" />
          <SearchBar datas={SearchJSONdata} searchClass="search-bar peligro" />
          <SearchBar datas={SearchJSONdata} searchClass="search-bar advertencia" />
          <SearchBar datas={SearchJSONdata} searchClass="search-bar valida" />
          <SearchBar datas={SearchJSONdata} searchClass="search-bar oscura" />
          <SearchBar datas={SearchJSONdata} searchClass="search-bar ligera" />
        </Center>
        <Center src={dragon} custom="Buttons">
          <ButtonsContainer />
        </Center>
        <Center src={dragon} custom="Avatars">
          <AvatarsContainer />
        </Center>
        <Center src={dragon} custom="Forms">
          <Form01 title="Register" formClass="form-01 peligro">
            <Span01>Name</Span01>
            <Input01 inputClass="input-01 primera" type="text" />
            <Span01>Email</Span01>
            <Input01 inputClass="input-01 advertencia" type="text" />
            <Span01>Password</Span01>
            <Input01 inputClass="input-01 oscura" type="password" />
            <Input01 inputClass="input-01 oscura" type="submit" />
            
          </Form01>
        </Center>
        <Center src={dragon} custom="Forms">
          <Form01 title="Register" formClass="form-01 oscura">
            <Span01>Name</Span01>
            <Input01 inputClass="input-01 primera" type="text" />
            <Span01>Email</Span01>
            <Input01 inputClass="input-01 peligro" type="text" />
            <Span01>Password</Span01>
            <Input01 inputClass="input-01 advertencia" type="password" />
            <Input01 inputClass="input-01 valida" type="submit" />
            
          </Form01>
        </Center>
        

    </>
  );
};

export default Tests;
